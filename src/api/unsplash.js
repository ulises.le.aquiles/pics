import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID _zkZh6sPUajPUyot2zC5g-7hGg_Wcsqmg3ny0mU2hR4'
    }
});